Travis-CI poking project
========================================
.. image:: https://img.shields.io/travis/berenm/poke-travis/linux.png
    :alt: Build status (linux branch)
    :target: https://travis-ci.org/berenm/poke-travis
.. image:: https://img.shields.io/travis/berenm/poke-travis/macosx.png
    :alt: Build status (macosx branch)
    :target: https://travis-ci.org/berenm/poke-travis

GOALS
````````````````````````````

This project contains some sample .travis.yml for bootstrapping C++11 on Travis-CI.


COPYING INFORMATION
````````````````````````````

 Distributed under the Boost Software License, Version 1.0.

 See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt
